#include "imageboard.h"
#include "mainwindow.h"


imageboard::imageboard(){}
imageboard::~imageboard(){}

void imageboard::downloadImages(const QString url, const QString saveLocation,const bool checkBox, QProgressBar *bar){
    QJsonObject jsonObject;
    qDebug()<<url;
    getJSON(url,jsonObject);

    //this connects the signals emmitted by the methods in this class to
    //the progressbar and the correct method needed.
    connect(this,SIGNAL(maxValue(int)),bar,SLOT(setMaximum(int)));
    connect(this,SIGNAL(progress(int)),bar,SLOT(setValue(int)));
    getNumofImages(jsonObject);

    QString folderName=makeTitle(jsonObject,checkBox,url);
    getNumofImages(jsonObject);

    //4chan json is a nested json
    QJsonValue value = jsonObject.value("posts");
    QJsonArray array = value.toArray();

    int num1 = getNthOccurence("/",url,3);
    QString url2 = url; //url is const so have to make another variable
    //Everything below is just to get the board. This might be moved to a separate method
    QString board = url2.remove(0,num1+1);
    int num2=getNthOccurence("/",board,1);
    board = board.remove(num2,board.length());

    //Checks and see if a folder with the structure [board]/[foldeName] exists and creates it
    //if it doesnt
    if(!QDir(saveLocation+"/"+board+"/"+folderName).exists()){
        QDir().mkpath(saveLocation+"/"+board+"/"+folderName);
    }

    //Goes through the jsnonArray and gets the UNIX timestamp and file extension
    //A counter is there so the progressbar can be updated.
    int count = 0;
    foreach (const QJsonValue &v, array){
        QString filename = v.toObject().value("filename").toString();
        QString extension = v.toObject().value("ext").toString();
        double t = v.toObject().value("tim").toDouble();
        QString tim = QString::number(t,'g',13);

        if(filename != "" && extension !=""){
            QString link = "https://i.4cdn.org/"+board+"/"+tim+extension;
            qDebug()<<link;
            downloadFile(link, tim, extension,saveLocation,folderName,board);
            count++;
        }
        emit progress(count);
     }

}

void imageboard::downloadFile(const QString &link, const QString &tim, const QString &extension,const QString &saveLocation, const QString &folderName, const QString &board){
    QFile file(saveLocation+"/"+board+"/"+folderName+"/"+tim+extension);
    if(!file.exists()){
        QNetworkAccessManager network;
        QNetworkRequest request;
        request.setUrl(QUrl(link));
        QNetworkReply *reply = network.get(request);

        QEventLoop loop;
        QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
        loop.exec();
        if(reply->error() != QNetworkReply::NoError){
            qDebug()<<"nothing there";
            delete reply;
            return;
        }
        QByteArray downloadFile =reply->readAll();

        file.open(QIODevice::WriteOnly);
        file.write(downloadFile);
        file.close();

        reply->deleteLater();
    }
}

//This method is here because the images field in the json
//isn't always updated to match the actual number of images
void imageboard::getNumofImages(const QJsonObject &Object){
    int num = 0;
    QJsonValue value = Object.value("posts");
    QJsonArray array = value.toArray();

    foreach (const QJsonValue &v, array){
        QString filename = v.toObject().value("filename").toString();

        if(filename != ""){
            num++;
        }

     }
    emit maxValue(num);
}

void imageboard::getJSON(const QString &url, QJsonObject &jsonObject){
    QNetworkAccessManager network;
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    QNetworkReply *reply = network.get(request);

    QEventLoop loop;
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
    if(reply->error() != QNetworkReply::NoError){
        qDebug()<<"no JSON";
        delete reply;
        return;
    }
    QString strData = (QString)reply->readAll();
    delete reply;
    QJsonDocument jsonData = QJsonDocument::fromJson(strData.toUtf8());
    jsonObject = jsonData.object();
}

//if the checkbox is checked, the folder name will be the thread ID
//otherwise, it'll be the subject of the thread. if no subject exists
//then the name would be the semantic_url
QString imageboard::makeTitle(QJsonObject &Object, const bool &checked, const QString &url){
    QString title ="";
    if(checked){
        //http(s)://a.4cdn.org/g/thread/54898555.json
        int num1 = url.lastIndexOf("/");
        title=url.mid(num1,9);
        return title;

    }
    else{
        QJsonValue value = Object.value("posts");
        QJsonArray array = value.toArray();
        QJsonValue firstValue= array.at(0);
        title = firstValue.toObject().value("sub").toString();
        if (title ==""){
            title = firstValue.toObject().value("semantic_url").toString();
            title.remove("/");
            return title;
        }
        else{
            title.remove("/");
            return title;
        }

    }
}

//this method returns the Nth occurence of a character in a string
int imageboard::getNthOccurence(const QString &word,const QString &url, const int &number){
    int count = 0;
    for(int i = 0; i<url.length();i++){
        if(word == url.mid(i,1)){
            count++;
        }
        if(count == number){
            return i;
        }
    }
    return -1;

}



