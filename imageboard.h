#include "mainwindow.h"
#include <QDir>
#include "ui_mainwindow.h"
#include <QUrl>
#include <QJsonObject>
#include <QJsonDocument>
#include <QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>
#include <QObject>
#include <QJsonArray>
#include <QStringRef>
#include <QFileInfo>
#include <QThread>

#ifndef IMAGEBOARD_H
#define IMAGEBOARD_H

class imageboard : public QObject{
    Q_OBJECT

public:
    imageboard();
    ~imageboard();
    void downloadImages(const QString url, const QString saveLocation, const bool checkBox, QProgressBar *bar);
    int getNthOccurence(const QString &letter, const QString &url, const int &number);
private:
    QString makeTitle(QJsonObject &Object, const bool &checked, const QString &url);
    void getJSON(const QString &url,QJsonObject &jsonObject);
    void downloadFile(const QString &link, const QString &tim, const QString &extension,const QString &saveLocation, const QString &folderName, const QString &board);
    void getNumofImages(const QJsonObject &Object);
signals:
    void progress(int num);
    void maxValue(int num);


};
#endif // IMAGEBOARD_H
