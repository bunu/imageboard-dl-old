#-------------------------------------------------
#
# Project created by QtCreator 2016-06-04T11:27:18
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ImageboardDownloader
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    imageboard.cpp

HEADERS  += mainwindow.h \
    imageboard.h

FORMS    += mainwindow.ui
