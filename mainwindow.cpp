#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "imageboard.h"

QString saveLocation ="";
bool checkBox=false;
imageboard *image = new imageboard();

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete image;
    delete ui;
}


void MainWindow::on_downloadButton_clicked()
{
    QString url = ui->threadEdit->text();
    QProgressBar* bar = ui->progressBar;

    int index = 0;
    //Checks if a valid link is inputted and the save location isn't blank
    if(url.contains("https://boards.4chan.org") && saveLocation != ""){
        url = url+"/";
        index = image->getNthOccurence("/",url,6);
        url = url.mid(0,index); //strips out the semantic url if it's there
        url.remove(0,24);    //removes everything but the board and thread number
        url.insert(0,QString("https://a.4cdn.org"));
        url = url + ".json";
        image->downloadImages(url,saveLocation,checkBox,bar);
    }
    else if(url.contains("http://boards.4chan.org") && saveLocation != ""){
        url = url+"/";
        index = image->getNthOccurence("/",url,6);
        url=url.mid(0,index);
        url.remove(0,23);
        url.insert(0,QString("http://a.4cdn.org"));
        url = url + ".json";
        image->downloadImages(url,saveLocation,checkBox,bar);
    }
    else{
        QMessageBox::information(this, tr("imageboard-dl"), tr("Please enter a valid 4chan url or save location") );
    }


}

void MainWindow::on_selectButton_clicked()
{
    saveLocation = QFileDialog::getExistingDirectory(this, tr("Open Directory"),"~/", QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks);
    ui->saveEdit->setText(saveLocation);
}

void MainWindow::on_checkBox_toggled(bool checked)
{
    checkBox=checked;
}

